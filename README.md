# CS 378 Lab2

All group members should use a branching scheme based off of their username (optional: followed by a dash and content). i.e., Alex should name his 'production' branch "Alexhwa". If he wants to make a working branch, one name he could use would be "Alexhwa-working". 

For branches based off of tickets, use the opposite strategy; use the ticket number followed by your username. i.e. if Daniel was working on ticket #4, he would create his branch as 4-dkillough. 

This way we can ensure commits stay on the correct branches until they're ready to be added to main. 
