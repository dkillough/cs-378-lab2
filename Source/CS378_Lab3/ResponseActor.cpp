// Fill out your copyright notice in the Description page of Project Settings.


#include "ResponseActor.h"
#include "TriggerActor.h"
#include "Runtime/Engine/Public/EngineGlobals.h"

// Sets default values
AResponseActor::AResponseActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	RootComponent = MeshComponent;

}

// Called when the game starts or when spawned
void AResponseActor::BeginPlay()
{
	Super::BeginPlay();
	
	if (TriggerActor) {
		TriggerActor->OnTriggerDelegate.AddDynamic(this, &AResponseActor::RespondToTrigger);
	}
}

// Called every frame
void AResponseActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AResponseActor::RespondToTrigger() {
	GEngine->AddOnScreenDebugMessage(-1, 2.0, FColor::Green, FString::Printf(TEXT("Trigger Delegate interacted!")));
	SetActorLocation(GetActorLocation() + GetActorForwardVector() * 500.0f);
}

